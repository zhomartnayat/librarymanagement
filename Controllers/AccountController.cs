﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.AuthorizeModels;

namespace WebApplication4.Controllers
{
    
    public class AccountController : Controller
    {
        public readonly UserManager<IdentityUser> userManager;
        public readonly SignInManager<IdentityUser> signinManager;
        public AccountController(UserManager<IdentityUser> userManager,SignInManager<IdentityUser> signinManager)
        {
            this.userManager = userManager;
            this.signinManager = signinManager;
        }

      
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(User model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);
               
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "User");
                    
                    if (signinManager.IsSignedIn(User) && User.IsInRole("Administrator")){
                        return RedirectToAction();
                    }
                  await signinManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("index", "home");
                }
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                } 
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signinManager.SignOutAsync();
            return RedirectToAction("index", "home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            HttpContext.Session.SetString("userEmail", "");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginUser model,string returnUrl)
        {
            if (ModelState.IsValid)
            {
               
                var result = await signinManager.PasswordSignInAsync(model.Email, model.Password,model.RememberMe,false);
                HttpContext.Session.SetString("userEmail", model.Email);
                if (result.Succeeded)
                {
                  
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }
                    else
                    {
                       
                        return RedirectToAction("index", "home");
                    }
                }
               
                    ModelState.AddModelError(string.Empty, "Login is invalid. Please, check your input data");
                 
            }
            return View(model);
        }
        [AllowAnonymous][HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

    }
}