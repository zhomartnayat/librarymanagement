﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.AuthorizeModels
{
    public class EditRoleModel
    {
        public EditRoleModel()
        {
            Users = new List<string>();
        }
        public string RoleId { get; set; }
        [Required]
        public string RoleName { get; set; }
        public List<string > Users { get; set; }
    }
}
