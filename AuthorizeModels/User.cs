﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.AuthorizeModels
{
    public class User
    {
        private string testingemail;
        private string testingpassword;

        public User(string testingemail, string testingpassword, string confirmpassword)
        {
            this.testingemail = testingemail;
            this.testingpassword = testingpassword;
            ConfirmPassword = confirmpassword;
        }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name ="Confirm Password")]
        [Compare("Password",ErrorMessage ="Password and Confirmation password does not match")]
        public string ConfirmPassword { get; set; }

    }
}
