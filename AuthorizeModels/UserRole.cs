﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.AuthorizeModels
{
    public class UserRole
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public bool isSelected { get; set; }
    }
}
