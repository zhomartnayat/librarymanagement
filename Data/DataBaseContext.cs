﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Data
{
    public class DataBaseContext:IdentityDbContext
    {
        public DataBaseContext(DbContextOptions options) : base(options)
        {

        } 
        public DbSet<Library> Library { get; set; }
        public DbSet<Members> Members { get; set; }
        public DbSet<Genres> Genres { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<Author> Author { get; set; }
        public DbSet<BorrowHistory> BorrowHistory { get; set; }
        public DbSet<BookAuthor> BookAuthor { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            
        }
    }
}
