﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Author:IValidatableObject
    {
        [Key]
        public int AuthorId { get; set; }
        [Display(Name="First Name"),Required, RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please"),MinLength(1,ErrorMessage ="Name should be more than 1 letter"),MaxLength(50,ErrorMessage ="Name cannot be more than 50 characters")]
        public string First_Name {get;set;}
        [Display(Name = "Second Name"), RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please"), MinLength(1, ErrorMessage = "Surname should be more than 1 letter"), MaxLength(50, ErrorMessage = "Surname cannot be more than 50 characters")]
        public string Second_Name { get; set; }
        [Remote("IsUserEmailAvailable", "Authors", HttpMethod = "POST", ErrorMessage = "Email is already exists in database.")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid."), MinLength(1, ErrorMessage = "Email should be more than 1 letter"), MaxLength(50, ErrorMessage = "Email cannot be more than 50 characters")]
        public string Email { get; set; }
        [MinLength(6, ErrorMessage = "Password should be more than 6 letter"), MaxLength(50, ErrorMessage = "Password cannot be more than 50 characters")]
        public string Password { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime date_of_birth { get; set; }
        public List<BookAuthor> BookAuhtor { get; set; }
        public List<Book> Book { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(this.First_Name))
            {
                errors.Add(new ValidationResult("First Name cannot be empty!", new [] { "First_Name" }));
            }
            if (string.IsNullOrWhiteSpace(this.Second_Name))
            {
                errors.Add(new ValidationResult("Second Name cannot be empty!", new[] { "Second_Name" }));
            }
            if (string.IsNullOrWhiteSpace(this.Email))
            {
                errors.Add(new ValidationResult("Email cannot be empty!", new[] { "Email" }));
            }
            if (string.IsNullOrWhiteSpace(this.Password))
            {
                errors.Add(new ValidationResult("Password cannot be empty!", new[] { "Password" }));
            }
            return errors;
        }

   

    }
}
