﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Genres
    {
        [Key]
        public int GenreId { get; set; }
        [Required(ErrorMessage ="Name is required")]
        [Display(Name = "Genre Name"), MinLength(1, ErrorMessage = "Name should be more than 1 letter"), MaxLength(50, ErrorMessage = "Name cannot be more than 50 characters")]
        public string Genre_Name { get; set; }
        public List<Book> Book { get; set; }
    }
}
