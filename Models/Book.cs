﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Book:IValidatableObject
    {
        [Key]
        public int BookId { get; set; }
        [Display(Name = "Book Title"), MinLength(1, ErrorMessage = "Title should be more than 1 letter"), MaxLength(50, ErrorMessage = "Title cannot be more than 50 characters")]
        public string Book_Name { get; set; }
        [Display(Name = "Book Description"),MinLength(1, ErrorMessage = "Description should be more than 1 letter")]
        public string Book_Desc { get; set; }
        [Display(Name = "Serial Number"), RegularExpression("([0-9]+)", ErrorMessage = "Use numbers only please"), MinLength(1, ErrorMessage = "Serial number should be more than 1 letter"), MaxLength(50, ErrorMessage = "SerialNumber cannot be more than 50 characters")]
        public string Serial_Number { get; set; }
        public int? GenreId { get; set; }
        public Genres Genres { get; set; }
        public int? LibraryId { get; set; }
        public Library Library { get; set; }
        public List<BorrowHistory> BorrowHistory { get; set; }
        public List<BookAuthor> BookAuhtor { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(this.Book_Name))
            {
                errors.Add(new ValidationResult("Title cannot be empty!", new[] { "Book_Name" }));
            }
            if (string.IsNullOrWhiteSpace(this.Book_Desc))
            {
                errors.Add(new ValidationResult("Description cannot be empty!", new[] { "Book_Desc" }));
            }
            if (string.IsNullOrWhiteSpace(this.Serial_Number))
            {
                errors.Add(new ValidationResult("Serial Number cannot be empty!", new[] { "Serial_Number" }));
            }
            return errors;
        }
    }
}
