﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class BorrowHistory
    {
        [Key]
        public int BorrowId { get; set; }
        public int? MemberId { get; set; }
        public Members Members { get; set; }
        public int? BookId { get; set; }
        public Book Book { get; set; }
        public DateTime Borrow_Date { get; set; }
        public DateTime Issue_Date { get; set; }
    }
}
