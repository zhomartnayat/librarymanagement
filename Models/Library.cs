﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Library
    {
        [Key]
        public int LibraryId { get; set; }
        [Required(ErrorMessage ="Name is required")]
        [Display(Name = "Library Name"), MinLength(1, ErrorMessage = "Name should be more than 1 letter"), MaxLength(50, ErrorMessage = "Name cannot be more than 50 characters")]
        public string LibraryName { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [Display(Name = "Library Address"), MinLength(1, ErrorMessage = "Address should be more than 1 letter"), MaxLength(50, ErrorMessage = "Address cannot be more than 50 characters")]
        public string LibraryAddress { get; set; }
        public List<Members> Members { get; set; }
        public List<Book> Book { get; set; }
    }
}
